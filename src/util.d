const(char*)[] strsToCSlice(const(string)[] strings) {
    const(char*)[] toReturn;    
    foreach (i; strings) {
        toReturn ~= (i ~ "\0").ptr;
    }
    return toReturn;
}
