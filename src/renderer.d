import util;
import std.exception,
       std.stdio,
       std.file,      
       std.conv;
import bindbc.sdl;
import erupted.vulkan_lib_loader,
       erupted;

class VulkanRenderer {
    private:
        bool initialised;
        VkInstance instance;
        VkPhysicalDevice physicalDevice;
        VkDevice device;
        int graphicsQueueIndex;
        VkQueue graphicsQueue;
        int presentQueueIndex;
        VkQueue presentQueue;

        SDL_Window* window;
        VkSurfaceKHR surface;
        VkSwapchainKHR swapchain;
        VkSurfaceFormatKHR swapchainFormat;
        VkImageView[] swapchainImages;
        VkFramebuffer[] swapchainFrames;
        uint winW = 1920;
        uint winH = 1080;

        VkPipeline graphicsPipeline;
        VkRenderPass renderPass;
        VkPipelineLayout pipelineLayout;
        VkCommandPool graphicsPool;
        VkCommandBuffer[] graphicsBuf;

        string[] extensions = [];
        auto validation = ["VK_LAYER_KHRONOS_validation"];
        auto deviceExtensions = ["VK_KHR_swapchain"];

        void choosePhysicalDevice() {
            VkPhysicalDevice[] devices;
            uint numDevices;
            vkEnumeratePhysicalDevices(instance, &numDevices, null);
            devices.length = numDevices;
            vkEnumeratePhysicalDevices(instance, &numDevices, devices.ptr);
            physicalDevice = devices[0];
        }

        void getQueueFamilyIndices() {
            //Get the index number of the closest graphics queue family
            uint numFamilies;
            VkQueueFamilyProperties[] properties;
            vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &numFamilies, null);
            properties.length = numFamilies;
            vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &numFamilies, properties.ptr);

            graphicsQueueIndex = -1;
            foreach (ref i,e; properties) {
                if (e.queueFlags | VK_QUEUE_GRAPHICS_BIT) {
                    graphicsQueueIndex = cast(int)i;
                    break;
                }
            }
            enforce(graphicsQueueIndex >= 0, "Failed to find a graphics queue family");

            //Get the index number of the closest WSI presentation queue family
            presentQueueIndex = -1;
            foreach (i; 0..properties.length) {
                VkBool32 compatible;
                vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice, cast(uint)i, surface, &compatible);
                if (compatible) {
                    presentQueueIndex = cast(int)i;
                    break;
                }
            }
            enforce(presentQueueIndex >= 0, "Failed to find a presentation queue family");
        }

        void createInstance() {
            VkInstanceCreateInfo instanceInfo;
            with (instanceInfo) {
                sType                   = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
                ppEnabledExtensionNames = strsToCSlice(extensions).ptr;
                enabledExtensionCount   = cast(uint)extensions.length;
                ppEnabledLayerNames     = strsToCSlice(validation).ptr;
                enabledLayerCount       = cast(uint)validation.length;
            }
            enforce(!vkCreateInstance(&instanceInfo, null, &instance), "Failed to create Vulkan instance");
            loadInstanceLevelFunctions(instance);
        }

        void createDevice() {
            choosePhysicalDevice;
            getQueueFamilyIndices;

            //Setup the queues to create
            VkDeviceQueueCreateInfo[] queueInfo;
            float priority = 1.0;
            queueInfo.length = graphicsQueueIndex == presentQueueIndex ? 1 : 2;

            foreach (i, ref e; queueInfo) with (e) {
                sType            = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
                queueCount       = 1;
                queueFamilyIndex = i ? presentQueueIndex : graphicsQueueIndex;
                pQueuePriorities = &priority;
            }

            //Create the logical device
            VkPhysicalDeviceFeatures features;
            VkDeviceCreateInfo deviceInfo;
            with (deviceInfo) {
                sType                   = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
                ppEnabledExtensionNames = strsToCSlice(deviceExtensions).ptr;
                enabledExtensionCount   = cast(uint)deviceExtensions.length;
                pEnabledFeatures        = &features;
                pQueueCreateInfos       = queueInfo.ptr;
                queueCreateInfoCount    = cast(uint)queueInfo.length;
            }
            enforce(!vkCreateDevice(physicalDevice, &deviceInfo, null, &device), "Failed to create Vulkan device");
            loadDeviceLevelFunctions(instance);

            //Get handles to queues of logical device
            vkGetDeviceQueue(device, graphicsQueueIndex, 0, &graphicsQueue);
            enforce(graphicsQueue, "Failed to obtain graphics queue");
            vkGetDeviceQueue(device, presentQueueIndex, 0, &presentQueue);
            enforce(presentQueue, "Failed to obtain present queue");
        }

        void createWindow() {
            //Create a window and get the SDL required extensions
            window = SDL_CreateWindow("Vulkan", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, winW, winH, SDL_WINDOW_VULKAN | SDL_WINDOW_SHOWN);
            enforce(window, "Failed to create window: " ~ to!string(SDL_GetError));

            uint numExtensions;
            SDL_Vulkan_GetInstanceExtensions(window, &numExtensions, null);
            const(char)*[] extensionNames;
            extensionNames.length = numExtensions;
            SDL_Vulkan_GetInstanceExtensions(window, &numExtensions, extensionNames.ptr);

            foreach (i; extensionNames) {
                extensions ~= to!string(i);
            }
        }

        void createSurface() {
            //Create the window surface
            SDL_Vulkan_CreateSurface(window, instance, &surface);
            enforce(surface, "Failed to create window surface: " ~ to!string(SDL_GetError));
        }

        void createSwapchain() {
            //Get the surface capabilities and possible formats
            VkSurfaceCapabilitiesKHR capabilities;
            vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, surface, &capabilities);
            
            VkSurfaceFormatKHR[] formats;
            uint numFormats;
            vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &numFormats, null);
            formats.length = numFormats;
            vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &numFormats, formats.ptr);
            swapchainFormat = formats[0];
            
            //Create the swapchain
            VkSwapchainCreateInfoKHR swapchainInfo;
            with (swapchainInfo) {
                sType                 = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
                surface               = this.surface;
                minImageCount         = capabilities.maxImageCount >= 3 ? 3 : capabilities.minImageCount;
                imageExtent           = VkExtent2D(winW, winH);
                imageFormat           = swapchainFormat.format;
                imageColorSpace       = swapchainFormat.colorSpace;
                imageUsage            = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
                imageArrayLayers      = 1;
                imageSharingMode      = graphicsQueueIndex != presentQueueIndex ? VK_SHARING_MODE_CONCURRENT : VK_SHARING_MODE_EXCLUSIVE;
                queueFamilyIndexCount = graphicsQueueIndex != presentQueueIndex ? 2 : 1;
                pQueueFamilyIndices   = graphicsQueueIndex != presentQueueIndex ? [uint(graphicsQueueIndex), uint(presentQueueIndex)].ptr : cast(uint*)&graphicsQueueIndex;
                presentMode           = VK_PRESENT_MODE_IMMEDIATE_KHR;
                clipped               = VK_TRUE;
            }
            enforce(!vkCreateSwapchainKHR(device, &swapchainInfo, null, &swapchain), "Failed to create swapchain"); 
        }

        void createRenderPass() {
            VkAttachmentDescription attachment;
            with (attachment) {
                format        = swapchainFormat.format;
                samples       = VK_SAMPLE_COUNT_1_BIT;
                loadOp        = VK_ATTACHMENT_LOAD_OP_CLEAR;
                storeOp       = VK_ATTACHMENT_STORE_OP_STORE;
                initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
                finalLayout   = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
            }

            VkAttachmentReference reference = VkAttachmentReference(0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);
            VkSubpassDescription subpass;
            with (subpass) {
                pipelineBindPoint    = VK_PIPELINE_BIND_POINT_GRAPHICS;
                colorAttachmentCount = 1;
                pColorAttachments    = &reference;
            }

            auto waitStages = [VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT];
            VkSubpassDependency dependency;
            with (dependency) {
                srcSubpass    = VK_SUBPASS_EXTERNAL;
                dstSubpass    = 0;
                srcStageMask  = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
                dstStageMask  = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
                dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
            }

            VkRenderPassCreateInfo renderPassInfo;
            with (renderPassInfo) {
                sType           = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
                attachmentCount = 1;
                pAttachments    = &attachment;
                subpassCount    = 1;
                pSubpasses      = &subpass;
                dependencyCount = 1;
                pDependencies   = &dependency;
            }

            enforce(!vkCreateRenderPass(device, &renderPassInfo, null, &renderPass), "Failed to create Vulkan render pass");
        }

        void createPipeline() {
            //Create shader modules and configure shader stages
            VkShaderModule[2] modules;
            VkPipelineShaderStageCreateInfo[2] stageInfo;

            foreach (i; 0..2) {
                VkShaderModuleCreateInfo moduleInfo;
                with (moduleInfo) {
                    ubyte[] obj;
                    obj.length = getSize(to!string(i) ~ ".o");
                    File(to!string(i) ~ ".o", "r").rawRead(obj);

                    sType    = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
                    pCode    = cast(uint*)obj.ptr;
                    codeSize = obj.length;

                    vkCreateShaderModule(device, &moduleInfo, null, &modules[i]);
                }

                with (stageInfo[i]) {
                    sType  = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
                    stage  = i ? VK_SHADER_STAGE_FRAGMENT_BIT : VK_SHADER_STAGE_VERTEX_BIT;
                    Module = modules[i];
                    pName  = "main\0".ptr;
                }
            }

            //Configure fixed-function stages and then create the graphics pipeline
            VkPipelineVertexInputStateCreateInfo vertexInput;
            with (vertexInput) {
                sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
            }

            VkPipelineInputAssemblyStateCreateInfo inputAssembly;
            with (inputAssembly) {
                sType    = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
                topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
            }

            VkViewport viewport = VkViewport(0, 0, winW, winH, 0.0, 1.0);
            VkRect2D scissor = VkRect2D(VkOffset2D(0,0), VkExtent2D(winW,winH));
            VkPipelineViewportStateCreateInfo viewportState;
            with (viewportState) {
                sType         = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
                viewportCount = 1;
                pViewports    = &viewport;
                scissorCount  = 1;
                pScissors     = &scissor;
            }

            VkPipelineRasterizationStateCreateInfo rasterizer;
            with (rasterizer) {
                sType          = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
                polygonMode    = VK_POLYGON_MODE_FILL;
                lineWidth      = 1.0;
                depthBiasClamp = 0.0;
                cullMode       = VK_CULL_MODE_BACK_BIT;
                frontFace      = VK_FRONT_FACE_CLOCKWISE;  
            }

            VkPipelineMultisampleStateCreateInfo multisample;
            with (multisample) {
                sType                = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
                rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
            }

            VkPipelineColorBlendAttachmentState attachmentBlend;
            with (attachmentBlend) {
                colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
            }
            VkPipelineColorBlendStateCreateInfo blendInfo;
            with (blendInfo) {
                sType           = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
                attachmentCount = 1;
                pAttachments    = &attachmentBlend;
            }

            VkPipelineLayoutCreateInfo pipelineLayoutInfo;
            pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
            enforce(!vkCreatePipelineLayout(device, &pipelineLayoutInfo, null, &pipelineLayout), "Failed to create Vulkan pipeline layout");

            createRenderPass;
            VkGraphicsPipelineCreateInfo pipelineInfo;
            with (pipelineInfo) {
                sType               = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
                stageCount          = 2;
                pStages             = stageInfo.ptr;
                layout              = pipelineLayout;
                pVertexInputState   = &vertexInput;
                pInputAssemblyState = &inputAssembly;
                pViewportState      = &viewportState;
                pRasterizationState = &rasterizer;
                pMultisampleState   = &multisample;
                pColorBlendState    = &blendInfo;
                renderPass          = this.renderPass;
                subpass             = 0;
            }
            enforce(!vkCreateGraphicsPipelines(device, null, 1, &pipelineInfo, null, &graphicsPipeline), "Failed to create Vulkan graphics pipeline");

            //Destroy shader modules
            foreach (i; modules) {
                vkDestroyShaderModule(device, i, null);
            }
        }

        void createSwapchainWrap() {
            //Create image views and framebuffers for each image of the swapchain
            VkImage[] images;
            uint numImages;
            vkGetSwapchainImagesKHR(device, swapchain, &numImages, null);
            images.length = numImages;
            vkGetSwapchainImagesKHR(device, swapchain, &numImages, images.ptr);
            
            swapchainImages.length = numImages;
            swapchainFrames.length = numImages;
            foreach (i,e; images) {
                VkImageViewCreateInfo viewInfo;
                with (viewInfo) {
                    sType      = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
                    viewType   = VK_IMAGE_VIEW_TYPE_2D;
                    format     = swapchainFormat.format;
                    image      = e;
                    components = VkComponentMapping(VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY);
                    with (subresourceRange) {
                        aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
                        levelCount = 1;
                        layerCount = 1;
                    }
                }
                enforce(!vkCreateImageView(device, &viewInfo, null, &swapchainImages[i]), "Failed to create image view");

                VkFramebufferCreateInfo frameInfo;
                with (frameInfo) {
                    sType           = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
                    renderPass      = this.renderPass;
                    attachmentCount = 1;
                    pAttachments    = &swapchainImages[i];
                    width           = winW;
                    height          = winH;
                    layers          = 1;
                }
                enforce(!vkCreateFramebuffer(device, &frameInfo, null, &swapchainFrames[i]), "Failed to create framebuffer");
            }      
        }

        void createCmdBuffers() {
            //Create command pool and allocate command buffers
            VkCommandPoolCreateInfo poolInfo;
            with (poolInfo) {
                sType            = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
                queueFamilyIndex = graphicsQueueIndex;
            }
            enforce(!vkCreateCommandPool(device, &poolInfo, null, &graphicsPool), "Failed to create Vulkan command pool");

            graphicsBuf.length = swapchainImages.length;
            VkCommandBufferAllocateInfo allocation;
            with (allocation) {
                sType              = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
                commandBufferCount = cast(uint)swapchainImages.length;
                commandPool        = graphicsPool;
            }
            vkAllocateCommandBuffers(device, &allocation, graphicsBuf.ptr);

            //Begin the recording of the command pool
            foreach (i,e; graphicsBuf) {
                VkCommandBufferBeginInfo bufferUsage;
                bufferUsage.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
                bufferUsage.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
                vkBeginCommandBuffer(e, &bufferUsage);
                vkCmdBindPipeline(e, VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline);
                
                VkClearValue clearValue;
                clearValue.color.float32 = [0,0,0,0];
                VkRenderPassBeginInfo instanceInfo;
                with (instanceInfo) {
                    sType           = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
                    renderPass      = this.renderPass;
                    framebuffer     = swapchainFrames[i];
                    renderArea      = VkRect2D(VkOffset2D(0,0), VkExtent2D(winW,winH));
                    clearValueCount = 1;
                    pClearValues    = &clearValue;
                }
                vkCmdBeginRenderPass(e, &instanceInfo, VK_SUBPASS_CONTENTS_INLINE);

                vkCmdDraw(e, 3, 1, 0, 0);

                vkCmdEndRenderPass(e);
                vkEndCommandBuffer(e);
            }
        }

    public:
        void init() {
            if (initialised) {
                deinit;
            }

            enforce(loadGlobalLevelFunctions, "Failed to load Vulkan library");
            createWindow;
            createInstance;
            createSurface;
            createDevice;
            createSwapchain;
            createPipeline;
            createSwapchainWrap;
            createCmdBuffers;
            initialised = true;
        }

        void deinit() {
            if (initialised) {
                vkDestroyCommandPool(device, graphicsPool, null);
                foreach (i; swapchainFrames) {
                    vkDestroyFramebuffer(device, i, null);
                }                
                foreach (i; swapchainImages) {
                    vkDestroyImageView(device, i, null);
                }
                vkDestroyPipeline(device, graphicsPipeline, null);
                vkDestroyPipelineLayout(device, pipelineLayout, null);
                vkDestroyRenderPass(device, renderPass, null);
                vkDestroySwapchainKHR(device, swapchain, null);
                vkDestroySurfaceKHR(instance, surface, null);
                vkDestroyDevice(device, null);
                vkDestroyInstance(instance, null);
            }
        }

        void run() {
            //Setup semaphores
            VkSemaphore[2] semaphores;
            foreach (ref i; semaphores) {
                VkSemaphoreCreateInfo semaphoreInfo;
                semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
                vkCreateSemaphore(device, &semaphoreInfo, null, &i);
            }

            //Main loop
            bool running = true;
            while (running) {
                //SDL event loop
                SDL_Event event;
                while (SDL_PollEvent(&event)) {
                    switch (event.type) {
                        case SDL_QUIT:
                            running = false;
                            break;
                        default:
                            break;
                    }
                }

                //Obtain an image, render, present
                uint imageIndex;
                vkAcquireNextImageKHR(device, swapchain, cast(ulong)1e9, semaphores[0], null, &imageIndex);
                
                VkPipelineStageFlags stage = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
                VkSubmitInfo submitInfo;
                with (submitInfo) {
                    waitSemaphoreCount   = 1;
                    pWaitSemaphores      = &semaphores[0];
                    signalSemaphoreCount = 1;
                    pSignalSemaphores    = &semaphores[1];
                    pWaitDstStageMask    = &stage;
                    commandBufferCount   = 1;
                    pCommandBuffers      = &graphicsBuf[imageIndex];
                }
                vkQueueSubmit(graphicsQueue, 1, &submitInfo, null);

                VkPresentInfoKHR presentInfo;
                with (presentInfo) {
                    sType              = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
                    waitSemaphoreCount = 1;
                    pWaitSemaphores    = &semaphores[1];
                    swapchainCount     = 1;
                    pSwapchains        = &swapchain;
                    pImageIndices      = &imageIndex;
                }
                vkQueuePresentKHR(presentQueue, &presentInfo);
            }

            //Destroy semaphores
            foreach (i; semaphores) {
                vkDestroySemaphore(device, i, null);
            }
        }
    
        this() {
            init;
        }

        ~this() {
            deinit;
        }
}
